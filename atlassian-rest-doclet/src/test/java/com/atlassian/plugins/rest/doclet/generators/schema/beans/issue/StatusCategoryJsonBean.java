package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A JSON-convertable representation of a StatusCategory
 *
 * @since v6.1
 */
@JsonIgnoreProperties (ignoreUnknown = true)
public class StatusCategoryJsonBean
{
    @JsonProperty
    private String self;

    @JsonProperty
    private Long id;

    @JsonProperty
    private String key;

    @JsonProperty
    private String colorName;

    @JsonProperty
    private String name;
}
