package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;

/**
 * @since v6.5
 */
public class SubclassedGeneric extends Generic<String, Long>
{
}

@JsonAutoDetect
class Generic<A, B>
{
    private A a;
    private B b;
}