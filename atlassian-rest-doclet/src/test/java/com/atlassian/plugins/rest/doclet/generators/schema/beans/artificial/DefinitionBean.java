package com.atlassian.plugins.rest.doclet.generators.schema.beans.artificial;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.xml.bind.annotation.XmlRootElement;

@JsonAutoDetect
public class DefinitionBean
{
    @XmlRootElement
    public static class ReusedBean {
        private String value;
        private ReusedBean rec;
    }

    private ReusedBean a;
    private ReusedBean b;

}
