package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 * Represents a group of links. Link groups contain:
 * <ul>
 *     <li>id: an optional identifier</li>
 *     <li>header: an optional styled header, e.g. for a dropdown trigger</li>
 *     <li>links: a list of simple links</li>
 *     <li>groups: link groups nested within this link group</li>
 * </ul>
 *
 * @since v5.0
 */
public class LinkGroupBean
{
    @XmlElement
    @JsonProperty
    private String id;

    @XmlElement
    @JsonProperty
    private String styleClass;

    @XmlElement
    @JsonProperty
    private SimpleLinkBean header;

    @XmlElement
    private Integer weight;

    @XmlElement
    @JsonProperty
    private final List<SimpleLinkBean> links = new ArrayList<SimpleLinkBean>();

    @XmlElement
    @JsonProperty
    private final List<LinkGroupBean> groups = new ArrayList<LinkGroupBean>();
}
