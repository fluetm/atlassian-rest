package com.atlassian.plugins.rest.doclet.generators.schema.beans.issue;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean representing a SimpleLink.  This is useful when constructing dropdowns populated dynamically.
 *
 * @since v5.0
 */
@SuppressWarnings ({ "UnusedDeclaration" })
@XmlRootElement (name = "link")
public class SimpleLinkBean
{
    @XmlElement
    private String id;

    @XmlElement
    private String styleClass;

    @XmlElement
    private String iconClass;

    @XmlElement
    private String label;

    @XmlElement
    private String title;

    @XmlElement
    private String href;

    @XmlElement
    private Integer weight;
}
