package com.atlassian.plugins.rest.doclet.generators.schema.beans.permissions;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Represents one permission and whether the caller has it
 *
 * @since v5.0
 */
public class UserPermissionJsonBean extends PermissionJsonBean
{
    @JsonProperty
    public boolean havePermission;

    @JsonProperty
    public Boolean deprecatedKey;
}
