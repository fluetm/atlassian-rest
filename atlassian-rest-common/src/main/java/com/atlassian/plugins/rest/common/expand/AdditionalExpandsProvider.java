package com.atlassian.plugins.rest.common.expand;

import java.util.List;

/**
 * A plugin point which can supply additional expands to be returned in the expand string when rendering the object.
 * To use this plugin point define a class implementing this interface, and publish it as an OSGi service in your plugin.
 */
public interface AdditionalExpandsProvider<T> {
    List<String> getAdditionalExpands(T entity);

    Class<T> getSupportedType();
}
