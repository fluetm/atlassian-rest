package com.atlassian.plugins.rest.common.security.jersey;

import java.lang.annotation.Annotation;
import java.util.List;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.ext.Provider;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugins.rest.common.security.CorsAllowed;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaults;
import com.atlassian.plugins.rest.common.security.descriptor.CorsDefaultsModuleDescriptor;

import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import org.springframework.beans.factory.DisposableBean;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * Factory for the Cross-Origin Resource Sharing resource filter, triggering off {@link com.atlassian.plugins.rest.common.security.CorsAllowed}.
 *
 * @since 2.6
 */
@Provider
public class CorsResourceFilterFactory implements ResourceFilterFactory, DisposableBean {
    private final PluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor> tracker;

    public CorsResourceFilterFactory(PluginAccessor pluginAccessor, PluginEventManager pluginEventManager) {
        tracker = new DefaultPluginModuleTracker<CorsDefaults, CorsDefaultsModuleDescriptor>(pluginAccessor, pluginEventManager, CorsDefaultsModuleDescriptor.class);
    }

    public List<ResourceFilter> create(final AbstractMethod method) {
        if (annotationIsPresent(method, CorsAllowed.class)) {
            String targetMethod = HttpMethod.GET;
            for (Annotation ann : method.getAnnotations()) {
                HttpMethod m = ann.annotationType().getAnnotation(HttpMethod.class);
                if (m != null) {
                    targetMethod = m.value();
                    break;
                }
            }

            ResourceFilter resourceFilter = new CorsResourceFilter(tracker, targetMethod);
            return singletonList(resourceFilter);

        } else {
            return emptyList();
        }
    }

    private static boolean annotationIsPresent(AbstractMethod method, Class<? extends Annotation> annotationType) {
        return method.isAnnotationPresent(annotationType)
                || method.getResource().isAnnotationPresent(annotationType)
                || packageHasAnnotation(annotationType, method.getResource().getResourceClass().getPackage());
    }

    private static boolean packageHasAnnotation(Class<? extends Annotation> annotationClass, Package resourcePackage) {
        return resourcePackage != null && resourcePackage.isAnnotationPresent(annotationClass);
    }

    public void destroy() {
        tracker.close();
    }
}