package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.web.context.HttpContext;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenValidator;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.api.model.AbstractResource;
import com.sun.jersey.spi.container.ResourceFilter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestXsrfResourceFilterFactory {
    private static final String LEGACY_FEATURE_KEY = "atlassian.rest.xsrf.legacy.enabled";

    @Mock
    private DarkFeatureManager darkFeatureManager;
    @Mock
    private HttpContext httpContext;
    private XsrfRequestValidator xsrfRequestValidator;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private PluginEventManager pluginEventManager;
    private XsrfResourceFilterFactory xsrfResourceFilterFactory;

    private interface RequiresXsrfCheck {
    }

    @Before
    public void setUp() {
        xsrfRequestValidator = new XsrfRequestValidatorImpl(
            new IndependentXsrfTokenValidator(
                new IndependentXsrfTokenAccessor()));
        when(darkFeatureManager.isFeatureEnabledForAllUsers(
            LEGACY_FEATURE_KEY)).thenReturn(false);

        xsrfResourceFilterFactory = new XsrfResourceFilterFactory(
            httpContext, xsrfRequestValidator, pluginAccessor, pluginEventManager, darkFeatureManager);
    }

    @Test
    public void testHasXsrfAnnotationWithRequiresXsrfCheckPresent() {
        AnnotatedElement annotatedElement = getMockAnnotatedElement(true);
        assertThat(xsrfResourceFilterFactory.hasRequiresXsrfCheckAnnotation(
            annotatedElement), is(true));
    }

    @Test
    public void testHasXsrfAnnotationWithoutRequiresXsrfCheckPresent() {
        AnnotatedElement annotatedElement = getMockAnnotatedElement(false);
        assertThat(xsrfResourceFilterFactory.hasRequiresXsrfCheckAnnotation(
            annotatedElement), is(false));
    }

    @Test
    public void testHasXsrfAnnotationWithRequiresXsrfCheckAnnotation() {
        Annotation[] annotations = {getMockAnnotation(Path.class),
            getMockAnnotation(
                com.atlassian.plugins.rest.common.security.RequiresXsrfCheck.class
            )};
        AnnotatedElement annotatedElement = getMockAnnotatedElement(false,
            annotations);
        assertThat(xsrfResourceFilterFactory.hasRequiresXsrfCheckAnnotation(
            annotatedElement), is(true));
    }

    @Test
    public void testHasXsrfAnnotationWithAnotherRequiresXsrfCheckAnnotation() {
        Annotation[] annotations = {getMockAnnotation(Path.class),
            getMockAnnotation(RequiresXsrfCheck.class)};
        AnnotatedElement annotatedElement = getMockAnnotatedElement(false,
            annotations);
        assertThat(xsrfResourceFilterFactory.hasRequiresXsrfCheckAnnotation(
            annotatedElement), is(true));
    }

    @Test
    public void testHasXsrfAnnotationWithoutRequiresXsrfCheckAnnotation() {
        Annotation[] annotations = {getMockAnnotation(Path.class),
            getMockAnnotation(Consumes.class)};
        AnnotatedElement annotatedElement = getMockAnnotatedElement(false,
            annotations);
        assertThat(xsrfResourceFilterFactory.hasRequiresXsrfCheckAnnotation(
            annotatedElement), is(false));
    }

    @Test
    public void testCreateWithXsrfProtectionExcludedAnnotation() {
        Annotation[] annotations = {getMockAnnotation(
            XsrfProtectionExcluded.class)};
        AbstractMethod abstractMethod = getMockAbstractMethod(annotations);
        assertThat(xsrfResourceFilterFactory.create(abstractMethod), empty());
    }

    @Test
    public void testCreateWithoutXsrfProtectionExcludedAnnotation() {
        AbstractMethod abstractMethod = getMockAbstractMethod(
            new Annotation[0]);
        assertThat(xsrfResourceFilterFactory.create(abstractMethod), hasSize(1));
    }

    @Test
    public void testCreateWithoutXsrfProtectionExcludedAnnotationInLegacyMode() {
        enableLegacyXsrfMode();
        AbstractMethod abstractMethod = getMockAbstractMethod(
            new Annotation[0]);
        assertThat(xsrfResourceFilterFactory.create(abstractMethod), empty());
    }

    @Test
    public void testCreateGetResourceWithRequiresXsrfCheckAnnotation() {
        Annotation[] annotations = {getMockAnnotation(
            RequiresXsrfCheck.class)};
        AbstractMethod abstractMethod = getMockAbstractMethod(annotations);
        when(abstractMethod.isAnnotationPresent(GET.class)).thenReturn(true);
        assertThat(xsrfResourceFilterFactory.create(abstractMethod), hasSize(1));
    }

    private void assertThatCreatePostProtectedByOriginBasedXsrfFilter(
        boolean containsOriginBasedXsrfFilter) {
        AbstractMethod abstractMethod = mock(AbstractMethod.class);
        AbstractResource abstractResource = getMockAbstractResource(
            false, new Annotation[0]);
        Annotation[] annotations = {getMockAnnotation(POST.class)};
        when(abstractMethod.getResource()).thenReturn(abstractResource);

        when(abstractMethod.getAnnotations()).thenReturn(annotations);
        when(abstractMethod.isAnnotationPresent(POST.class)).thenReturn(true);

        List<ResourceFilter> resourceFilters = xsrfResourceFilterFactory.create(
            abstractMethod);
        if (containsOriginBasedXsrfFilter) {
            assertThat(resourceFilters,
                contains(instanceOf(OriginBasedXsrfResourceFilter.class)));
        } else {
            assertThat(resourceFilters,
                not(contains(instanceOf(OriginBasedXsrfResourceFilter.class))));
        }
    }

    @Test
    public void testCreatePostProtectedByOriginBasedXsrfFilter() {
        assertThatCreatePostProtectedByOriginBasedXsrfFilter(false);
    }

    @Test
    public void testCreatePostProtectedByOriginBasedXsrfFilterInLegacyMode() {
        enableLegacyXsrfMode();
        assertThatCreatePostProtectedByOriginBasedXsrfFilter(true);
    }

    @Test
    public void testCreateGetNotProtectedByOriginBasedXsrfFilter() {
        AbstractMethod abstractMethod = mock(AbstractMethod.class);
        AbstractResource abstractResource = getMockAbstractResource(
            false, new Annotation[0]);
        Annotation[] annotations = {getMockAnnotation(GET.class)};
        when(abstractMethod.getResource()).thenReturn(abstractResource);

        when(abstractMethod.getAnnotations()).thenReturn(annotations);
        when(abstractMethod.isAnnotationPresent(GET.class)).thenReturn(true);

        List<ResourceFilter> resourceFilters = xsrfResourceFilterFactory.create(
            abstractMethod);

        assertThat(resourceFilters, empty());
    }

    // The OriginBasedXsrfFilter checks the request method before running
    // its xsrf checks.
    @Test(expected = AssertionError.class)
    public void testCreateGetHasOriginBasedXsrfFilterInLegacyMode() {
        enableLegacyXsrfMode();
        testCreateGetNotProtectedByOriginBasedXsrfFilter();
    }

    @Test
    public void testCreateGetResourceWithRequiresXsrfCheckAnnotationInLegacyMode() {
        enableLegacyXsrfMode();
        testCreateGetResourceWithRequiresXsrfCheckAnnotation();
    }

    @Test
    public void testCreateGetResourceWithoutRequiresXsrfCheckAnnotation() {
        AbstractMethod abstractMethod = getMockAbstractMethod(
            new Annotation[0]);
        when(abstractMethod.isAnnotationPresent(GET.class)).thenReturn(true);
        assertThat(xsrfResourceFilterFactory.create(abstractMethod), empty());
    }

    @Test
    public void testCreateGetResourceWithoutRequiresXsrfCheckAnnotationInLegacyMode() {
        enableLegacyXsrfMode();
        testCreateGetResourceWithoutRequiresXsrfCheckAnnotation();
    }

    private void enableLegacyXsrfMode() {
        when(darkFeatureManager.isFeatureEnabledForAllUsers(
            LEGACY_FEATURE_KEY)).thenReturn(true);
    }

    private AbstractMethod getMockAbstractMethod(Annotation[] annotations) {
        AbstractMethod abstractMethod = mock(AbstractMethod.class);
        AbstractResource abstractResource = mock(AbstractResource.class);
        when(abstractMethod.getResource()).thenReturn(abstractResource);
        when(abstractMethod.getAnnotations()).thenReturn(annotations);
        when(abstractResource.getAnnotations()).thenReturn(annotations);
        return abstractMethod;
    }

    private AnnotatedElement getMockAnnotatedElement(
        boolean xsrfAnnotationPresent) {
        return getMockAnnotatedElement(xsrfAnnotationPresent,
            new Annotation[]{});
    }

    private AnnotatedElement getMockAnnotatedElement(
        boolean xsrfAnnotationPresent, Annotation[] annotations) {
        return setupAnnotatedElement(mock(AnnotatedElement.class),
            xsrfAnnotationPresent, annotations);
    }

    private AnnotatedElement setupAnnotatedElement(
        AnnotatedElement annotatedElement, boolean xsrfAnnotationPresent,
        Annotation[] annotations) {
        when(annotatedElement.isAnnotationPresent(
            com.atlassian.plugins.rest.common.security.RequiresXsrfCheck.class)
        ).thenReturn(xsrfAnnotationPresent);
        when(annotatedElement.getAnnotations()).thenReturn(annotations);
        return annotatedElement;
    }

    private AbstractResource getMockAbstractResource(
        boolean xsrfAnnotationPresent, Annotation[] annotations) {
        AbstractResource abstractResource = mock(AbstractResource.class);
        when(abstractResource.getAnnotations()).thenReturn(annotations);
        when(abstractResource.isAnnotationPresent(
            com.atlassian.plugins.rest.common.security.RequiresXsrfCheck.class)
        ).thenReturn(xsrfAnnotationPresent);
        return abstractResource;
    }

    private Annotation getMockAnnotation(final Class clazz) {
        Annotation annotation = mock(Annotation.class);
        when(annotation.annotationType()).thenAnswer(
            new Answer<Class<? extends Annotation>>() {
                public Class<? extends Annotation> answer(InvocationOnMock invoc) {
                    return clazz;
                }
            });
        return annotation;
    }
}