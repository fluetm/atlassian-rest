package com.atlassian.plugins.rest.common.security.jersey;

import com.atlassian.plugins.rest.common.security.AuthenticationRequiredException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
public class TestSecurityExceptionMapper {
    private SecurityExceptionMapper mapper;

    @Mock
    private Request request;


    @Before
    public void setupMocks() {
        mapper = new SecurityExceptionMapper();
        mapper.request = request;
    }

    @Test
    public void exceptionAlwaysMapsToUnauthorized() {
        SecurityException e = new AuthenticationRequiredException();
        Response resp = mapper.toResponse(e);
        assertEquals("Response status should be unauthorized", 401, resp.getStatus());
    }
}
