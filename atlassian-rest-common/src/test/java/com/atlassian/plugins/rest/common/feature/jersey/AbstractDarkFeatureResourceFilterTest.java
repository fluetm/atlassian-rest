package com.atlassian.plugins.rest.common.feature.jersey;

import com.atlassian.plugins.rest.common.feature.RequiresDarkFeature;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.api.model.AbstractResource;
import com.sun.jersey.spi.container.ContainerRequest;
import org.junit.Before;
import org.mockito.Mock;

import java.lang.annotation.Annotation;

import static com.atlassian.plugins.rest.common.feature.jersey.test.DarkFeatureTestResource.FEATURE_KEY;
import static com.atlassian.plugins.rest.common.feature.jersey.test.DarkFeatureTestResource.getAnnotatedMethod;
import static com.atlassian.plugins.rest.common.feature.jersey.test.DarkFeatureTestResource.getNonAnnotatedMethod;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public abstract class AbstractDarkFeatureResourceFilterTest {
    @Mock
    protected AbstractMethod abstractMethod;

    @Mock
    protected AbstractResource resource;

    @Mock
    protected DarkFeatureManager darkFeatureManager;

    @Mock
    protected ContainerRequest request;

    @Mock
    protected RequiresDarkFeature annotation;

    @Before
    public void setup() {
        initMocks(this);

        when(abstractMethod.getResource()).thenReturn(resource);
        doReturn(RequiresDarkFeature.class).when(annotation).annotationType();

        setResourceAnnotated(false);
        setMethodAnnotated(false);
        setResourceAnnotation(FEATURE_KEY);
    }

    protected void setFeatureEnabled(final boolean enabled) {
        setFeatureEnabled(FEATURE_KEY, enabled);
    }

    protected void setFeatureEnabled(final String key, final boolean enabled) {
        when(darkFeatureManager.isFeatureEnabledForCurrentUser(eq(key))).thenReturn(enabled);
    }

    protected void setResourceAnnotated(final boolean annotated) {
        when(resource.getAnnotation(eq(RequiresDarkFeature.class))).thenReturn(annotated ? annotation : null);
        when(resource.getAnnotations()).thenReturn(annotated ? new Annotation[]{annotation} : new Annotation[0]);
    }

    protected void setResourceAnnotation(final String... keys) {
        when(annotation.value()).thenReturn(keys);
    }

    protected void setMethodAnnotated(final boolean annotated) {
        when(abstractMethod.getAnnotations()).thenReturn(annotated ? new Annotation[]{annotation} : new Annotation[0]);
        when(abstractMethod.getMethod()).thenReturn(annotated ? getAnnotatedMethod() : getNonAnnotatedMethod());
    }
}
