package com.google.common.base;

public class SuppliersTestUtils {
    public static void resetMemoizingSupplier(Supplier supplier) {
        if (supplier instanceof Suppliers.MemoizingSupplier) {
            Suppliers.MemoizingSupplier memoizingSupplier = (Suppliers.MemoizingSupplier) supplier;
            memoizingSupplier.initialized = false;
        } else {
            throw new IllegalArgumentException("Passed supplier is not of MemoizingSupplier type");
        }
    }
}
