package com.atlassian.plugins.rest.sample.helloworld;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/*
 * This resource serves paths that start with /hello as per the {@link Path Path("/hello")} annotation. The {@link AnonymousAllowed} ensures the resource is accessible for user who have not
 * authenticated.
 */
@Path("/hello")
@AnonymousAllowed
public class HelloWorld {
    /*
     * This method explained:
     * <ul>
     * <li>{@link GET}, it accepts HTTP GET requests</li>
     * <li>{@link Produces Produces("text/plain")}, the HTTP response content type will be {@code text/plain}</li>
     * <li>{@link Path Path("{name}")}, this method will serve path like /hello/<name> where name can be any valid path element. It will be then passed as a parameter to the parameter annotated with
     * {@link PathParam PathParam("name")}</li>
     * </ul>
     */
    @GET
    @Produces("text/plain")
    @Path("{name}")
    public String sayHelloTo(@PathParam("name") String name) {
        return "Hello " + name;
    }
}