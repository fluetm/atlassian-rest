package com.atlassian.plugins.rest.sample.entities;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.ArrayList;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Providers;
import javax.xml.bind.JAXBContext;
import javax.xml.parsers.SAXParserFactory;

import com.sun.jersey.core.impl.provider.entity.XMLRootObjectProvider;
import com.sun.jersey.spi.inject.Injectable;

import org.junit.Test;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.reset;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EntityTest {
    private EntityResource entity = new EntityResource();

    @Test
    public void testApplesForOranges() {
        HttpHeaders headers = createMock(HttpHeaders.class);
        expect(headers.getRequestHeader("Content-Type")).andReturn(new ArrayList<String>()).once();
        expect(headers.getRequestHeader("Accept")).andReturn(new ArrayList<String>()).once();
        replay(headers);
        Apple apple = (Apple) entity.applesForOranges(new Orange("valencia"), headers).getEntity();
        assertEquals("apple-valencia", apple.getName());
        assertEquals("N/A", apple.getReqAccept());
        assertEquals("N/A", apple.getReqContentType());
        verify(headers);

        reset(headers);
        expect(headers.getRequestHeader("Content-Type")).andReturn(new ArrayList<String>() {{
            add("application/json");
        }}).once();
        expect(headers.getRequestHeader("Accept")).andReturn(new ArrayList<String>() {{
            add("application/xml");
        }}).once();
        replay(headers);
        apple = (Apple) entity.applesForOranges(new Orange("delfino"), headers).getEntity();
        assertEquals("apple-delfino", apple.getName());
        assertEquals("application/json", apple.getReqContentType());
        assertEquals("application/xml", apple.getReqAccept());
        verify(headers);

        reset(headers);
        expect(headers.getRequestHeader("Content-Type")).andReturn(new ArrayList<String>() {{
            add("application/json");
        }}).once();
        expect(headers.getRequestHeader("Accept")).andReturn(new ArrayList<String>() {{
            add("application/xml");
            add("application/json");
            add("image/png");
        }}).once();
        replay(headers);
        apple = (Apple) entity.applesForOranges(new Orange("delfino"), headers).getEntity();
        assertEquals("apple-delfino", apple.getName());
        assertEquals("application/json", apple.getReqContentType());
        assertEquals("application/xml,application/json,image/png", apple.getReqAccept());
        verify(headers);
    }

    private static InputStream streamOf(String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

    @Test
    public void unmarshallingFromStringsUsesXmlAttributeValues() throws Exception {
        JAXBContext ctxt = JAXBContext.newInstance(Pear.class);

        Providers providers = mock(Providers.class);
        ContextResolver<JAXBContext> ctxtResolver = mock(ContextResolver.class);
        when(ctxtResolver.getContext(Pear.class)).thenReturn(ctxt);
        when(providers.getContextResolver(JAXBContext.class, MediaType.APPLICATION_XML_TYPE)).thenReturn(ctxtResolver);

        Injectable<SAXParserFactory> spf = new Injectable<SAXParserFactory>() {
            public SAXParserFactory getValue() {
                SAXParserFactory inst = SAXParserFactory.newInstance();
                inst.setNamespaceAware(true);
                return inst;
            }
        };

        XMLRootObjectProvider rop = new XMLRootObjectProvider.App(spf, providers);

        Pear p1 = (Pear) rop.readFrom((Class) Pear.class, null, new Annotation[0],
                MediaType.APPLICATION_XML_TYPE, null,
                streamOf("<pear name='Bartlett'/>"));

        assertEquals("Bartlett", p1.getName());

        Pear p2 = (Pear) rop.readFrom((Class) Pear.class, null, new Annotation[0],
                MediaType.APPLICATION_XML_TYPE, null,
                streamOf("<pear name='Conference'/>"));

        assertEquals("Conference", p2.getName());
    }
}
