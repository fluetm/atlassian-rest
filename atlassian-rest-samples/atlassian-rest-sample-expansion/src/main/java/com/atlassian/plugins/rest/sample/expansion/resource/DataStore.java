package com.atlassian.plugins.rest.sample.expansion.resource;

import com.atlassian.plugins.rest.sample.expansion.entity.Player;
import com.atlassian.plugins.rest.sample.expansion.entity.PlayerRecord;
import com.atlassian.plugins.rest.sample.expansion.entity.Game;
import com.atlassian.plugins.rest.sample.expansion.entity.Players;
import com.atlassian.plugins.rest.sample.expansion.entity.SubRecord;

import javax.ws.rs.core.UriBuilder;
import java.util.*;

/**
 * Simulates a data storage layer.  It provides an interface to load the raw data and convert it in to the REST
 * level API objects.
 *
 * This datastore knows of two players, one of which has a record and one that doesn't.  Its intended to be used
 * by the {@link PlayerResource} to show an example on how to optionally show fields for expansion.
 */
public class DataStore {
    private static final DataStore singleton = new DataStore();

    public static DataStore getInstance() {
        return singleton;
    }

    private Map<String, Game> games = new HashMap<String, Game>();
    private Map<Integer, String> playerNames = new TreeMap<Integer, String>();
    private Map<Integer, Integer> pointsScored = new HashMap<Integer, Integer>();

    private DataStore() {
        playerNames.put(0, "Adam Ashley-Cooper");
        playerNames.put(1, "Matt Giteau");
        playerNames.put(2, "Stephen Larkham");
        playerNames.put(3, "George Gregan");
        playerNames.put(4, "Mark Gerrard");
        playerNames.put(5, "George Smith");
        playerNames.put(6, "Stirling Mortlock");
        playerNames.put(7, "Clyde Rathbone");
        playerNames.put(8, "Jeremy Paul");
        playerNames.put(9, "Mark Chisholm");
        pointsScored.put(2, 513);
        games.put("rugby", new Game("Rugby"));
    }

    public Game getGame(String name, UriBuilder uriBuilder) {
        Game game = games.get(name);
        game.setPlayers(new Players(playerNames.size(), 5, uriBuilder));
        return game;
    }

    public List<Player> getPlayers(UriBuilder uriBuilder) {
        final List<Player> players = new ArrayList<Player>();
        for (Map.Entry<Integer, String> player : playerNames.entrySet()) {
            players.add(new Player(player.getKey(), player.getValue(), uriBuilder));
        }
        return players;
    }


    public Player getPlayer(int id, UriBuilder builder) {
        Player player = new Player(id, playerNames.get(id), builder);
        if (playerHasRecord(id)) {
            player.setRecord(PlayerRecord.emptyRecord(player));
        }
        return player;
    }

    public String getPlayerName(int id) {
        return playerNames.get(id);
    }

    /**
     * This simulates the use of a cheap call to a database to check the existence of potentially more expensive
     * data.  This is being used to detirmine if the client can see the expensive element at all and expand it.
     *
     * @param id
     * @return
     */
    private boolean playerHasRecord(int id) {
        return pointsScored.containsKey(id);
    }

    /**
     * This simulates the lookup of the expensive data element about the given player.  In this case it simulates
     * the action of performing a query on a series of tables and calculating the total points scored by this player.
     *
     * @param player
     * @return
     */
    public PlayerRecord getPlayerRecord(Player player) {
        PlayerRecord playerRecord = new PlayerRecord(this.pointsScored.get(player.getId()));
        playerRecord.setSubRecord1(SubRecord.emptySubRecord(playerRecord));
        playerRecord.setSubRecord2(SubRecord.emptySubRecord(playerRecord));
        return playerRecord;
    }

    public SubRecord getSubRecord(final PlayerRecord playerRecord) {
        return new SubRecord(playerRecord.getPointsScored() / 2);
    }
}
