package com.atlassian.plugins.rest.header;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import static com.atlassian.plugins.rest.common.security.jersey.AntiSniffingResponseFilter.ANTI_SNIFFING_HEADER_NAME;
import static com.atlassian.plugins.rest.common.security.jersey.AntiSniffingResponseFilter.ANTI_SNIFFING_HEADER_VALUE;

@Path("/header")
@AnonymousAllowed
@Produces({"text/plain"})
public class CustomisedHeaderResource {

    @GET
    @Path("/nosniff-container")
    public Response getResponseWithNoSniffAddedToContainerResponse() {
        return Response.status(200)
                .header(ANTI_SNIFFING_HEADER_NAME, ANTI_SNIFFING_HEADER_VALUE)
                .build();
    }

    @GET
    @Path("/nosniff-servlet")
    public Response getResponseWithNoSniffAddedToServletResponse(@Context HttpServletResponse response) {
        response.setHeader(ANTI_SNIFFING_HEADER_NAME, ANTI_SNIFFING_HEADER_VALUE);

        return Response.status(200).build();
    }
}
