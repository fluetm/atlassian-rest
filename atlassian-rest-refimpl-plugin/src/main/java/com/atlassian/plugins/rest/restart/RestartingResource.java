package com.atlassian.plugins.rest.restart;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugins.rest.autowiring.SomeService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.lang.Validate;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/restart")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class RestartingResource {
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;
    public static final String V2_KEY = "com.atlassian.plugins.rest.atlassian-rest-refimpl-plugin-v2";

    public RestartingResource(PluginController pluginController, PluginAccessor pluginAccessor) {
        this.pluginController = pluginController;
        this.pluginAccessor = pluginAccessor;
    }

    @POST
    @AnonymousAllowed
    public Response restartV2Plugin() {
        if (!pluginAccessor.isPluginEnabled(V2_KEY)) {
            throw new RuntimeException("Plugin isn't enabled");
        }
        pluginController.disablePlugin(V2_KEY);
        if (pluginAccessor.isPluginEnabled(V2_KEY)) {
            throw new RuntimeException("Plugin should be disabled");
        }
        pluginController.enablePlugins(V2_KEY);
        if (!pluginAccessor.isPluginEnabled(V2_KEY)) {
            throw new RuntimeException("Plugin isn't enabled");
        }
        return Response.ok().build();
    }
}