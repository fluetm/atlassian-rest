package com.atlassian.plugins.rest.test;

import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.expand.Expandable;
import com.atlassian.plugins.rest.common.expand.Expander;
import com.google.common.collect.ImmutableList;

import javax.ws.rs.core.UriInfo;

import static javax.xml.bind.annotation.XmlAccessType.*;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

@XmlRootElement
@XmlAccessorType(FIELD)
@Expander(DeveloperExpander.class)
public class Developer {
    @XmlAttribute
    private String expand;

    @XmlElement
    private Link link;

    @XmlAttribute
    private String userName;

    @XmlElement
    private String fullName;

    @XmlElement
    private String email;

    @XmlElement
    @Expandable("drinks")
    private FavouriteDrinks favouriteDrinks;

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFavouriteDrinks(FavouriteDrinks favouriteDrinks) {
        this.favouriteDrinks = favouriteDrinks;
    }

    public List<FavouriteDrink> getFavouriteDrinks() {
        return favouriteDrinks.getFavouriteDrinks() != null ? ImmutableList.copyOf(favouriteDrinks.getFavouriteDrinks()) : Collections.<FavouriteDrink>emptyList();
    }

    public static Developer getDeveloper(String userName, UriInfo uriInfo) {
        final Developer developer = new Developer();
        developer.setLink(Link.self(uriInfo.getAbsolutePathBuilder().path("developer").path(userName).build()));
        developer.setUserName(userName);
        return developer;
    }
}
