package it.com.atlassian.rest.common;

import javax.ws.rs.core.MediaType;

import com.atlassian.rest.jersey.client.WebResourceFactory;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ExceptionMapperTest {
    WebResource anonymousWebResource = WebResourceFactory.anonymous().path("/");

    ClientResponse getSecurityFailureResponseFromPartialBuilder(WebResource.Builder builder) {
        try {
            builder.get(String.class);
            fail();
            throw new RuntimeException("Shouldn't happen after a fail()");
        } catch (UniformInterfaceException e) {
            return e.getResponse();
        }
    }

    private static void assertResponseIs(ClientResponse resp, ClientResponse.Status expectedStatus, MediaType expectedType) {
        assertEquals(expectedStatus, resp.getClientResponseStatus());
        assertEquals(expectedType, resp.getType());
    }

    @Test
    public void unauthorisedErrorsAreSentAsXmlByDefault() {
        ClientResponse resp = getSecurityFailureResponseFromPartialBuilder(anonymousWebResource.type("text/plain"));
        assertResponseIs(resp, ClientResponse.Status.UNAUTHORIZED, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void unauthorisedErrorsAreSentAsXmlWhenAnythingIsAccepted() {
        ClientResponse resp = getSecurityFailureResponseFromPartialBuilder(anonymousWebResource.accept("*/*"));
        assertResponseIs(resp, ClientResponse.Status.UNAUTHORIZED, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void unauthorisedErrorsAreSentAsXmlWhenRequested() {
        ClientResponse resp = getSecurityFailureResponseFromPartialBuilder(anonymousWebResource.accept(MediaType.APPLICATION_XML_TYPE));
        assertResponseIs(resp, ClientResponse.Status.UNAUTHORIZED, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void unauthorisedErrorsAreSentAsJsonWhenRequested() {
        ClientResponse resp = getSecurityFailureResponseFromPartialBuilder(anonymousWebResource.accept(MediaType.APPLICATION_JSON_TYPE));
        assertResponseIs(resp, ClientResponse.Status.UNAUTHORIZED, MediaType.APPLICATION_JSON_TYPE);
    }

    private static WebResource uncaughtErrorResource() {
        return WebResourceFactory.anonymous().path("errors/uncaughtInternalError");
    }

    @Test
    public void uncaughtExceptionSentAsXmlByDefault() {
        assertResponseIs(
                uncaughtErrorResource().get(ClientResponse.class),
                ClientResponse.Status.INTERNAL_SERVER_ERROR, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void uncaughtExceptionSentAsXmlWhenAnythingIsAccepted() {
        assertResponseIs(
                uncaughtErrorResource().accept("*/*").get(ClientResponse.class),
                ClientResponse.Status.INTERNAL_SERVER_ERROR, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void uncaughtExceptionSentAsXmlWhenRequested() {
        assertResponseIs(
                uncaughtErrorResource().accept(MediaType.APPLICATION_XML_TYPE).get(ClientResponse.class),
                ClientResponse.Status.INTERNAL_SERVER_ERROR, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void uncaughtExceptionSentAsJsonWhenRequested() {
        assertResponseIs(
                uncaughtErrorResource().accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class),
                ClientResponse.Status.INTERNAL_SERVER_ERROR, MediaType.APPLICATION_JSON_TYPE);
    }

    private static WebResource notFound() {
        return WebResourceFactory.authenticated().path("somepaththatdoesntexist");
    }

    @Test
    public void notFoundErrorsAreSentAsXmlByDefault() {
        ClientResponse resp = getSecurityFailureResponseFromPartialBuilder(notFound().type("text/plain"));
        assertResponseIs(resp, ClientResponse.Status.NOT_FOUND, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void notFoundErrorsAreSentAsXmlWhenAnythingIsAccepted() {
        ClientResponse resp = getSecurityFailureResponseFromPartialBuilder(notFound().accept("*/*"));
        assertResponseIs(resp, ClientResponse.Status.NOT_FOUND, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void notFoundErrorsAreSentAsXmlWhenRequested() {
        ClientResponse resp = getSecurityFailureResponseFromPartialBuilder(notFound().accept(MediaType.APPLICATION_XML_TYPE));
        assertResponseIs(resp, ClientResponse.Status.NOT_FOUND, MediaType.APPLICATION_XML_TYPE);
    }

    @Test
    public void notFoundErrorsAreSentAsJsonWhenRequested() {
        ClientResponse resp = getSecurityFailureResponseFromPartialBuilder(notFound().accept(MediaType.APPLICATION_JSON_TYPE));
        assertResponseIs(resp, ClientResponse.Status.NOT_FOUND, MediaType.APPLICATION_JSON_TYPE);
    }
}
