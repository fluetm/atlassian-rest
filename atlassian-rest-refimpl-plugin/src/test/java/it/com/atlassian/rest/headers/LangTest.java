package it.com.atlassian.rest.headers;

import static org.junit.Assert.*;

import org.junit.Test;

import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.ClientResponse;

public class LangTest {
    @Test
    public void testUMM49codes() {
        // UM M.49 language code could contains numbers,
        // for example es-419, see CONF-29922
        ClientResponse response = WebResourceFactory.anonymous(WebResourceFactory.REST_VERSION)
                .path("helloworld").path("locale")
                .acceptLanguage("es-419")
                .get(ClientResponse.class);

        // Jersey 1.19 behavior is to return 400, because it fails to parse
        // incoming request headers. Some parts will result in 500, for example like
        // ExceptionMapper in the original issue, however bug root cause is the same
        assertEquals(200, response.getStatus());
    }
}
