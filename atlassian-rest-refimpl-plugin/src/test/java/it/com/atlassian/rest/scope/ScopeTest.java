package it.com.atlassian.rest.scope;

import com.atlassian.fugue.Either;
import com.atlassian.rest.jersey.client.WebResourceFactory;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import org.junit.Test;

import javax.ws.rs.core.Cookie;
import java.util.Optional;

import static com.atlassian.rest.jersey.client.WebResourceFactory.REST_VERSION;
import static java.util.Optional.of;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;
import static org.junit.Assert.assertEquals;

public class ScopeTest {

    final Cookie dummyScope = new Cookie("atlassian.scope.dummy", "true");
    final Cookie refappScope = new Cookie("atlassian.scope.refapp", "true");

    @Test
    public void testExplicitScope() {
        assertEquals(Integer.valueOf(PRECONDITION_FAILED.getStatusCode()), doRequest("explicit-scope", Optional.empty()).left().get());

        assertEquals(Integer.valueOf(PRECONDITION_FAILED.getStatusCode()), doRequest("explicit-scope", of(dummyScope)).left().get());

        assertEquals("exp", doRequest("explicit-scope", of(refappScope)).right().get());
    }

    @Test
    public void testImplicitScope() {
        assertEquals(Integer.valueOf(PRECONDITION_FAILED.getStatusCode()), doRequest("implicit-scope", Optional.empty()).left().get());

        assertEquals(Integer.valueOf(PRECONDITION_FAILED.getStatusCode()), doRequest("implicit-scope", of(dummyScope)).left().get());

        assertEquals("imp", doRequest("implicit-scope", of(refappScope)).right().get());
    }

    @Test
    public void testNoScoped() {
        assertEquals("non", doRequest("non-scoped", Optional.empty()).right().get());

        assertEquals("non", doRequest("non-scoped", of(dummyScope)).right().get());

        assertEquals("non", doRequest("non-scoped", of(refappScope)).right().get());
    }

    private Either<Integer, String> doRequest(String path, Optional<Cookie> scope) {
        try {
            final WebResource ws = WebResourceFactory.anonymous(
                    WebResourceFactory.getUriBuilder()
                            .path("rest").path(path).path(REST_VERSION).path("get").build());
            return scope.isPresent()
                    ? Either.right(ws.cookie(scope.get()).get(String.class))
                    : Either.right(ws.get(String.class));
        } catch (UniformInterfaceException e) {
            return Either.left(e.getResponse().getStatusInfo().getStatusCode());
        }
    }
}
