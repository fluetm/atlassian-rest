package com.atlassian.plugins.rest.module.filter;

import com.atlassian.plugins.rest.common.security.CorsHeaders;
import com.atlassian.plugins.rest.common.security.jersey.CorsResourceFilter;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.ext.Provider;

import static com.atlassian.plugins.rest.common.security.jersey.CorsResourceFilter.extractOrigin;

/**
 * This is a filter to force Jersey to handle OPTIONS when part of a preflight cors check.
 *
 * @since 2.6
 */
@Provider
public class CorsAcceptOptionsPreflightFilter implements ContainerRequestFilter {
    public ContainerRequest filter(final ContainerRequest request) {
        if (request.getMethod().equals(HttpMethod.OPTIONS)) {
            String origin = extractOrigin(request);
            String targetMethod = request.getHeaderValue(CorsHeaders.ACCESS_CONTROL_REQUEST_METHOD.value());
            if (targetMethod != null && origin != null) {
                request.setMethod(targetMethod);
                request.getProperties().put(CorsResourceFilter.CORS_PREFLIGHT_REQUESTED, "true");
            }
        }
        return request;
    }
}
