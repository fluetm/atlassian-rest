package com.atlassian.plugins.rest.module;

import com.google.common.base.Function;

import javax.annotation.Nullable;

/**
 * Please add new functionality to the superclass so other modules can use it.
 * This class should be moved to atlassian-rest-common but it remains here for
 * backward compatibility
 */
public class ApiVersion extends com.atlassian.plugins.rest.common.version.ApiVersion {
    public static final String NONE_STRING = com.atlassian.plugins.rest.common.version.ApiVersion.NONE_STRING;
    public static final ApiVersion NONE = new ApiVersion(NONE_STRING);

    private static InvalidVersionException invalidVersionException(String version) {
        return new InvalidVersionException(version);
    }

    public ApiVersion(String version) {
        super(version, new Function<String, RuntimeException>() {
            @Nullable
            @Override
            public RuntimeException apply(String input) {
                return invalidVersionException(input);
            }
        });
    }
}
