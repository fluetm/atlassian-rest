package com.atlassian.plugins.rest.module.scope;

import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.plugins.rest.module.RestModuleDescriptor;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;

/**
 * Rejects requests that do not satisfy Scope checks.
 * <p>
 * A request is rejected if it requires Scope protection,
 * but associated tenant/user does not have scope activated.
 *
 * @since 3.2
 */
public class ScopeResourceFilter implements ResourceFilter, ContainerRequestFilter {
    private static final Logger log = LoggerFactory.getLogger(ScopeResourceFilter.class);

    private final RestModuleDescriptor descriptor;
    private final ScopeManager scopeManager;

    private final Response.Status failureStatus = PRECONDITION_FAILED;

    public ScopeResourceFilter(ScopeManager scopeManager, RestModuleDescriptor descriptor) {
        this.descriptor = descriptor;
        this.scopeManager = scopeManager;
    }

    /**
     * Proceeds with request if module's scope is activated for given tenant
     */
    public ContainerRequest filter(final ContainerRequest request) {
        log.debug("Applying scope filter for {} ", descriptor);

        final Boolean permit = descriptor.getScopeKey().map(scopeManager::isScopeActive).orElse(true);

        if (!permit) {
            log.debug("Scope is not active for matching descriptor {}", descriptor);

            throw new ScopeCheckFailedException(failureStatus);
        }

        return request;
    }

    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    public ContainerResponseFilter getResponseFilter() {
        return null;
    }
}
