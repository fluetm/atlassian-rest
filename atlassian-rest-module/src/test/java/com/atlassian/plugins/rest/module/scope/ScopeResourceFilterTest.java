package com.atlassian.plugins.rest.module.scope;

import com.atlassian.plugin.scope.ScopeManager;
import com.atlassian.plugins.rest.module.RestModuleDescriptor;
import com.sun.jersey.spi.container.ContainerRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScopeResourceFilterTest {

    @Mock
    ScopeManager scopeManager;

    @Mock
    RestModuleDescriptor restModuleDescriptor;

    @Mock
    ContainerRequest request;

    @InjectMocks
    public ScopeResourceFilter scopeResourceFilter;

    @Test
    public void testReturnRequestIfNoScopeInDescriptor() throws Exception {
        when(restModuleDescriptor.getScopeKey()).thenReturn(Optional.<String>empty());

        assertThat(request, equalTo(scopeResourceFilter.filter(request)));
    }

    @Test
    public void testReturnRequestIfScopeIsActive() throws Exception {
        when(restModuleDescriptor.getScopeKey()).thenReturn(Optional.of("jsd"));
        when(scopeManager.isScopeActive("jsd")).thenReturn(true);

        assertThat(request, equalTo(scopeResourceFilter.filter(request)));
    }


    @Test(expected = ScopeCheckFailedException.class)
    public void testFailRequestIfScopeIsNotActive() throws Exception {
        when(restModuleDescriptor.getScopeKey()).thenReturn(Optional.of("jsd"));
        when(scopeManager.isScopeActive("jsd")).thenReturn(false);

        assertThat(request, equalTo(scopeResourceFilter.filter(request)));
    }
}