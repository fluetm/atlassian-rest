package com.atlassian.plugins.rest.module.filter;

import com.atlassian.plugins.rest.common.security.CorsHeaders;
import com.atlassian.plugins.rest.common.security.jersey.CorsResourceFilter;
import com.sun.jersey.spi.container.ContainerRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.HttpMethod;
import java.util.HashMap;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class CorsAcceptOptionsPreflightFilterTest {
    private static final String ORIGIN = "https://example.onion";

    private CorsAcceptOptionsPreflightFilter corsAcceptOptionsPreflightFilter;

    @Mock
    private ContainerRequest request;

    @Mock
    private HashMap<String, Object> requestProperties;

    @Before
    public void setUp() {
        corsAcceptOptionsPreflightFilter = new CorsAcceptOptionsPreflightFilter();
    }

    @Test
    public void setsRequestCorsPreflightRequestedPropertyForCorsPreflightRequest() {
        final String requestedMethod = HttpMethod.POST;
        when(request.getMethod()).thenReturn(HttpMethod.OPTIONS);
        when(request.getProperties()).thenReturn(requestProperties);
        setOriginHeaderInRequest(request, ORIGIN);
        setAccessControlRequestMethodInRequest(request, requestedMethod);

        corsAcceptOptionsPreflightFilter.filter(request);

        verify(request, times(1)).setMethod(requestedMethod);
        verify(requestProperties, times(1)).put(
                CorsResourceFilter.CORS_PREFLIGHT_REQUESTED, Boolean.TRUE.toString());
    }

    private void setOriginHeaderInRequest(ContainerRequest request, String origin) {
        when(request.getHeaderValue(CorsHeaders.ORIGIN.value())).thenReturn(origin);
    }

    private void setAccessControlRequestMethodInRequest(
            ContainerRequest request, String method) {
        when(request.getHeaderValue(
                CorsHeaders.ACCESS_CONTROL_REQUEST_METHOD.value())).thenReturn(method);
    }
}