# Atlassian REST Plugin Module

## Description

You can use the REST plugin module to create plugin points easily in Atlassian applications, by
exposing services and data entities as REST APIs. The Atlassian REST plugin module is bundled with
our applications.

REST APIs provide access to resources via URI paths. To use a REST API, your plugin or script will
make an HTTP request and parse the response. You can choose JSON or XML for the response format.
Your methods will be the standard HTTP methods like GET, PUT, POST and DELETE. Because the REST API
is based on open standards, you can use any web development language to access the API.

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/REST).

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/REST).

### Documentation

[REST API Developer Documentation](https://developer.atlassian.com/display/DOCS/REST+API+Development)
